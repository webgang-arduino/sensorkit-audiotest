#include "SCD30.h"
#include "Arduino_SensorKit.h"
U8X8_SSD1306_128X64_NONAME_HW_I2C Oled2;

#define LED 6
#define BUZZER 5
#define CO2LIMIT 925

#if defined(ARDUINO_ARCH_AVR)
    #pragma message("Defined architecture for ARDUINO_ARCH_AVR.")
    #define SERIAL Serial
#elif defined(ARDUINO_ARCH_SAM)
    #pragma message("Defined architecture for ARDUINO_ARCH_SAM.")
    #define SERIAL SerialUSB
#elif defined(ARDUINO_ARCH_SAMD)
    #pragma message("Defined architecture for ARDUINO_ARCH_SAMD.")
    #define SERIAL SerialUSB
#elif defined(ARDUINO_ARCH_STM32F4)
    #pragma message("Defined architecture for ARDUINO_ARCH_STM32F4.")
    #define SERIAL SerialUSB
#else
    #pragma message("Not found any architecture.")
    #define SERIAL Serial
#endif

void setup() {
    SERIAL.begin(115200);
    SERIAL.println("SCD30 Raw Data");
    Oled2.begin();
    Oled2.setFlipMode(true); // rotation of the screen
    Oled2.setFont(u8x8_font_chroma48medium8_r); 
    Oled2.setCursor(0, 0);    // Set the Coordinates in char
    Oled2.print("-- CO2 v.012 --");
    Oled2.setCursor(0, 2);
    Oled2.print("Alarm at:");
    Oled2.print(CO2LIMIT);
    Oled2.setCursor(0, 4);
    Oled2.print("webgang 02/2022");
    Oled2.setCursor(0, 6);
    Oled2.print("SCD30=");
    Oled2.print(SCD30_I2C_ADDRESS,HEX);
    Oled2.print("; OLED=");
    //pins
    pinMode(LED,OUTPUT);
    pinMode(BUZZER,OUTPUT);
    Wire.begin();
    scd30.initialize();

// test Oled after scd30 init:
   // Oled2.begin();
    //Oled2.setFlipMode(true);
    //Oled2.setFont(u8x8_font_7x14B_1x2_r);
    
//    Oled2.clear();
//    Oled2.setCursor(0, 6);    // Set the Coordinates 
//    Oled2.print("CO2: ");
//    Oled2.print(0); 
//    Oled2.print(" ppm ");
//    Oled2.refreshDisplay();

    scd30.setMeasurementInterval(10);
    SERIAL.print("i2c ADDR:");
    SERIAL.println(SCD30_I2C_ADDRESS,HEX);
    SERIAL.print("OLED:i2c ADDR");
//    SERIAL.println(SeeedOLED_Address,HEX);
    }

void loop() {
    float result[3] = {0};

    if (scd30.isAvailable()) {
        scd30.getCarbonDioxideConcentration(result);
        SERIAL.print("Carbon Dioxide Concentration is: ");
        SERIAL.print(int(result[0]));
        SERIAL.println(" ppm");
  //      scd30.stopMeasurement();
        delay(2000);
// OLED
// does not work anymore after initialisation of scd30
   //     Oled2.setCursor(0, 2);    // Set the Coordinates 
        Oled2.setCursor(0, 6);
        Oled2.print("         ");
        Oled2.setCursor(0, 6);
        Oled2.print("CO2: ");
        Oled2.print(int(result[0])); 
        Oled2.print(" ppm  ");
        Oled2.refreshDisplay();

        if (result[0] > CO2LIMIT) {
            digitalWrite(LED, HIGH);
            tone(BUZZER, 150);
            delay(500);
            tone(BUZZER, result[0]);
            delay(500);
            noTone(BUZZER);
        }
    }
    delay(2000);
    tone(BUZZER, 80);
    delay(100);
    noTone(BUZZER);
    digitalWrite(LED, LOW);

} 
